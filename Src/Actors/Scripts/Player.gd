extends KinematicBody2D

<<<<<<< HEAD
onready var joystick = get_parent().get_node("Hud/Joystick/Joystick/Joystick_Button")
onready var fps_label = get_parent().get_node("Hud/FPS")
=======
# Gives script access to joystick lane
onready var joystick = get_parent().get_node("Hud/Joystick/Joystick_Base/Joystick_Button")
# Variables
>>>>>>> 62e4606010c7886bf1d19d463e60cab4b7b2d45f
var speed = 250
var velocity = Vector2.ZERO
var state_machine
var attacks = ["attack1","attack2"]


var touch_attack = false
var touch_big_attack = false

func _ready() -> void:
	state_machine = $AnimationTree.get("parameters/playback")

# Function to get input
func get_input():
	var current = state_machine.get_current_node()
	velocity = Vector2.ZERO
	## Action Input
	if Input.is_action_just_pressed("Big Attack") or touch_big_attack == true:
		state_machine.travel("attack1 2")
		return
	if Input.is_action_just_pressed("Attack") or touch_attack == true:
		state_machine.travel(attacks[randi() % 2])
		return
	if Input.is_action_just_pressed("Die"):
		die()
		return
	if Input.is_action_just_pressed("Hurt"):
		hurt()
		return
	# Keyboard Input
	## Movement Input
	if Input.is_action_pressed("Walk_Up"):
		velocity.y -= 1
	if Input.is_action_pressed("Walk_Down"):
		velocity.y += 1
	if Input.is_action_pressed("Walk_Left"):
		velocity.x -= 1
	if Input.is_action_pressed("Walk_Right"):
		velocity.x += 1
	# Touch Input
	if joystick.get_value() > Vector2.ZERO:
		velocity = joystick.get_value()
	if joystick.get_value() < Vector2.ZERO:
		velocity = joystick.get_value()
	
	# Normalizing to keep speed equal
	velocity = velocity.normalized()
	
	# Determines Weather player is running or idle
	if velocity.length() != 0:
		state_machine.travel("run")
	if velocity.length() == 0:
		state_machine.travel("idle")

# warning-ignore:unused_argument
func _physics_process(delta: float) -> void:
	get_input()
	fps_label.set_text(str(Engine.get_frames_per_second()))
	velocity = move_and_slide(velocity * speed)

# Tells state machine to play hurt when player is attacked
func hurt():
	state_machine.travel("hurt")

# Tells state machine to play die when player is died
func die():
	state_machine.travel("die")
	set_physics_process(false)

# Tells if palyer pressed the On-Screen Button
func _on_Attack_pressed() -> void:
	touch_attack = true

# Tells if palyer pressed the On-Screen Button
func _on_Big_Attack_pressed() -> void:
	touch_big_attack = true

# Tells if palyer released the On-Screen Button
func _on_Attack_released() -> void:
	touch_attack = false

# Tells if palyer released the On-Screen Button
func _on_Big_Attack_released() -> void:
	touch_big_attack = false
